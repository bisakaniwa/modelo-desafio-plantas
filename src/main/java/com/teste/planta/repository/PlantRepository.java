package com.teste.planta.repository;

import com.teste.planta.model.Plant;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PlantRepository extends CrudRepository<Plant, Long> {

    Optional<Plant> findByName(String name);

    List<Plant> findByType(String type);

    @Query("select p from Plant p where p.name like %:search% or p.type like %:search% or p.id like %:search%")
    List<Plant> searchAny(@Param("search") String search);
}
