package com.teste.planta;

import com.teste.planta.model.Plant;
import com.teste.planta.repository.PlantRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class PlantApplication {

	public static void main(String[] args) {
		SpringApplication.run(PlantApplication.class, args);
	}

	@Bean
	public CommandLineRunner plant(PlantRepository repository) {
		return (args) -> {
			//save
//				Plant palmeira = new Plant("Palmeira Chamaedorea", "Folhagem");
//				Plant darkPrince = new Plant("Echeveria Dark Prince", "Suculenta");
//				Plant limoeiro = new Plant("Limoeiro", "Árvore frutífera");
//				Plant serissa = new Plant("Serissa foetida", "Bonsai");
//				Plant hibisco = new Plant("Hibisco", "Arbusto");
//				Plant salgueiro = new Plant("Salgueiro chorão", "Árvore ornamental");
//				Plant hortela = new Plant("Hortelã", "Hortaliça");
//				Plant orquidea = new Plant("Orquídea dendrobium", "Flor");
//				Plant tostao = new Plant("Tostão rosa", "Semi-suculenta");
//				Plant jabuticabeira = new Plant("Jabuticabeira", "Árvore frutífera");
//				repository.save(palmeira);
//				repository.save(darkPrince);
//				repository.save(limoeiro);
//				repository.save(serissa);
//				repository.save(hibisco);
//				repository.save(salgueiro);
//				repository.save(hortela);
//				repository.save(orquidea);
//				repository.save(tostao);
//				repository.save(jabuticabeira);

			//fetch all
//			System.out.println(repository.findAll());
//
//			//fetch by ID
//			System.out.println(repository.findById(1L));
//			System.out.println(repository.findById(2L));
//			System.out.println(repository.findById(3L));
//			System.out.println(repository.findById(4L));
//			System.out.println(repository.findById(5L));
//			System.out.println(repository.findById(6L));
//			System.out.println(repository.findById(7L));
//			System.out.println(repository.findById(8L));
//			System.out.println(repository.findById(9L));
//			System.out.println(repository.findById(10L));

			//fetch by name
//			System.out.println(repository.findByName("Palmeira Chamaedorea"));
//			System.out.println(repository.findByName("Echeveria Dark Prince"));
//			System.out.println(repository.findByName("Limoeiro"));
//			System.out.println(repository.findByName("Serissa foetida"));
//			System.out.println(repository.findByName("Hibisco"));
//			System.out.println(repository.findByName("Salgueiro chorão"));
//			System.out.println(repository.findByName("Hortelã"));
//			System.out.println(repository.findByName("Orquídea dendrobium"));
//			System.out.println(repository.findByName("Tostão rosa"));
//			System.out.println(repository.findByName("Jabuticabeira"));

			//fetch by type
//			System.out.println(repository.findByType("Folhagem"));
//			System.out.println(repository.findByType("Suculenta"));
//			System.out.println(repository.findByType("Árvore frutífera"));
//			System.out.println(repository.findByType("Bonsai"));
//			System.out.println(repository.findByType("Arbusto"));
//			System.out.println(repository.findByType("Árvore ornamental"));
//			System.out.println(repository.findByType("Hortaliça"));
//			System.out.println(repository.findByType("Flor"));
//			System.out.println(repository.findByType("Semi-suculenta"));

			//update plant
//			Plant palmeira2 = repository.findById(1L).orElse(null);
//			palmeira2.setName("Palmeira ráfis");
//			repository.save(palmeira2);
//			System.out.println(palmeira2);

			//delete plant
//			repository.deleteById(1L);
//			repository.deleteById(2L);
//			repository.deleteById(3L);
//			repository.deleteById(4L);
//			repository.deleteById(5L);
//			repository.deleteById(6L);
//			repository.deleteById(7L);
//			repository.deleteById(8L);
//			repository.deleteById(9L);
//			repository.deleteById(10L);
//			System.out.println(repository.findAll());
		};
	}
}
