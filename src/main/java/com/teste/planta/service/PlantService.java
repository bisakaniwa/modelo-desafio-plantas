package com.teste.planta.service;

import com.teste.planta.model.Plant;
import com.teste.planta.repository.PlantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.stereotype.Service;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

@Service
public class PlantService {

    @Autowired
    private PlantRepository repository;

    public Plant save(Plant plant) {
        Plant newSave = new Plant();
        newSave.setName(plant.getName());
        newSave.setType(plant.getType());
        this.repository.save(newSave);
        return newSave;
    }

    public List<Plant> viewAll() {
        return (List<Plant>) this.repository.findAll();
    }

    public Plant byId(Long id) throws ChangeSetPersister.NotFoundException {
        Optional<Plant> plant = this.repository.findById(id);
        if (plant.isPresent()) {
            return plant.get();
        } else {
            throw new ChangeSetPersister.NotFoundException();
        }
    }

    public Plant byName(String name) throws ChangeSetPersister.NotFoundException {
        Optional<Plant> plant = this.repository.findByName(name);
        if (plant.isPresent()) {
            return plant.get();
        } else {
            throw new ChangeSetPersister.NotFoundException();
        }
    }

    public List<Plant> byType(String type) {
        return this.repository.findByType(type);
    }

    public List<Plant> searchAny(String search) {
        return this.repository.searchAny(search);
    }

    public Plant updateData(Plant plant) throws Exception {
        Optional<Plant> update = this.repository.findById(plant.getId());
        if (update.isPresent()) {
            update.get().setName(plant.getName());
            update.get().setType(plant.getType());
            return this.repository.save(update.get());
        } else {
            throw new Exception("Oops! Something went wrong :(");
        }
    }

    public void deleteById(Long id) {
        this.repository.deleteById(id);
    }
}

