package com.teste.planta.controller;

import com.teste.planta.model.Plant;
import com.teste.planta.service.PlantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/plant")
@CrossOrigin
public class PlantController {

    @Autowired
    private PlantService service;

    @GetMapping
    ResponseEntity<List<Plant>> findAll() {
        return ResponseEntity.ok(this.service.viewAll());
    }

    @GetMapping("/{id}")
    ResponseEntity<Plant> byId(@PathVariable Long id) throws ChangeSetPersister.NotFoundException {
        return ResponseEntity.ok(service.byId(id));
    }

    @GetMapping("/name/{name}")
    ResponseEntity<Plant> byName(@PathVariable String name) throws ChangeSetPersister.NotFoundException {
        return ResponseEntity.ok(service.byName(name));
    }

    @GetMapping("/type/{type}")
    ResponseEntity<List<Plant>> byType(@PathVariable String type) {
        return ResponseEntity.ok(service.byType(type));
    }

    @GetMapping("/search/{search}")
    ResponseEntity<List<Plant>> searchAny(@PathVariable String search) {
        return ResponseEntity.ok(this.service.searchAny(search));
    }

    @PostMapping
    ResponseEntity<Plant> save(@RequestBody Plant plant) {
        return new ResponseEntity<>(this.service.save(plant), HttpStatus.CREATED);
    }

    @PutMapping("/update")
    ResponseEntity<Plant> updateData(@RequestBody Plant plant) throws Exception {
        return ResponseEntity.ok(this.service.updateData(plant));
    }

    @DeleteMapping("/{id}")
    ResponseEntity<Plant> deleteById(@PathVariable Long id) {
    this.service.deleteById(id);
    return ResponseEntity.noContent().build();
    }
}
